import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import MyComponent from './MyComponent';

ReactDOM.render(
    <MyComponent cosa="yepe">
      <span key="hell yeah">cosa 1</span>
      <span key="Odin">cosa 2</span>
    </MyComponent>,
  document.getElementById('root')
);
