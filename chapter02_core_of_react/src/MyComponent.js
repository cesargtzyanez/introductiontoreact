/**
 * Created by César Gutiérrez Yáñez on 3/9/17.
 */

import React from 'react';

const SetIntervalMixin = {
  componentWillMount: function () {
    this.intervals = [];
  },
  setInterval: function () {
    this.intervals.push(setInterval.apply(null,arguments));
  },
  componentWillUnmount: function () {
    this.intervals.map(clearInterval);
  }
};

const MyComponent = React.createClass({
  displayName: "MyTickTockComponent",
  mixins: [SetIntervalMixin],//Here we add the mixin!
  getInitialState: function () {
    return {seconds: 0}
  },
  componentDidMount: function () {
    this.setInterval(this.tick,1000);
  },
  tick: function () {
    this.setState({seconds: this.state.seconds + 1});
  },
  render: function () {
    return (
        <h1>Running for {this.state.seconds}</h1>
    );
  }
});

export default MyComponent
